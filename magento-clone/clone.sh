SRC_DIR="/www/production/htdocs"
DEST_DIR="/www/staging/htdocs"

IGNORE_DIRS="
  var/cache
  var/chances
  var/session
"

SRC_DB=""
SRC_DB_USER=""
SRC_DB_PASS=""

DEST_DB=""
DEST_DB_USER=""
DEST_DB_PASS=""

DEST_URL="http://staging.chances.mventory.io/"

IGNORE_DIRS_=""

for DIR in $IGNORE_DIRS
do
  IGNORE_DIRS_=$IGNORE_DIRS_" --exclude="$DIR
done

rm -r $DEST_DIR
mkdir $DEST_DIR
chown ubuntu:www-data $DEST_DIR
chmod 775 $DEST_DIR

tar --create --directory $SRC_DIR $IGNORE_DIRS_ . | tar --extract --directory $DEST_DIR

#Restore magento config file
cp -rp $DEST_DIR/../scripts/clone-data/* $DEST_DIR

mysql -u $DEST_DB_USER -p$DEST_DB_PASS --execute "drop database $DEST_DB; create database $DEST_DB character set 'utf8'"

mysqldump $SRC_DB -u $SRC_DB_USER -p$SRC_DB_PASS | mysql $DEST_DB -u $DEST_DB_USER -p$DEST_DB_PASS

mysql $DEST_DB -u $DEST_DB_USER -p$DEST_DB_PASS --execute "update core_config_data set value = \"$DEST_URL\" where path like \"web/%/base_url\";"
mysql $DEST_DB -u $DEST_DB_USER -p$DEST_DB_PASS --execute "update core_config_data set value = 1 where path = \"design/head/demonotice\";"
