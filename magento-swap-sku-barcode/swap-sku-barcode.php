<?php

//This script makes swap of SKU and barcode values in all products
//Check the code before running it, it might require an adjustment.

//Change current directory to the directory of current script
chdir(dirname(__FILE__));

require 'app/Mage.php';

if (!Mage::isInstalled()) {
  echo 'Application is not installed yet, please complete install wizard first.';
  exit;
}

//Only for urls
//Don't remove this
$_SERVER['SCRIPT_NAME'] = str_replace(
  basename(__FILE__),
  'index.php',
  $_SERVER['SCRIPT_NAME']
);

$_SERVER['SCRIPT_FILENAME'] = str_replace(
  basename(__FILE__),
  'index.php',
  $_SERVER['SCRIPT_FILENAME']
);

Mage::app('admin')->setUseSessionInUrl(false);
Mage::setIsDeveloperMode(true);

ini_set('display_errors', 1);

umask(0);

try {

$products = $collection = Mage::getResourceModel('catalog/product_collection')
  ->addAttributeToSelect('product_barcode_')
  ->addAttributeToFilter(
      'type_id',
      Mage_Catalog_Model_Product_Type::TYPE_SIMPLE
    )
  ->load();

$skus = [];
$barcodes = [];
$numberOfBarcodes = 0;

foreach ($products as $product) {
  $skus[$product['sku']] = $product['entity_id'];

  if ($product['product_barcode_']) {
    $barcodes[$product['product_barcode_']] = $product['entity_id'];
    $numberOfBarcodes++;
  }
}

if (count($barcodes) != $numberOfBarcodes)
  exit('Barcodes are not unique so can\'t be used as SKUs' . PHP_EOL);

$intersectedBarcodes = array_intersect_key($barcodes, $skus);
$uniqueBarcodes = array_diff_assoc($barcodes, $intersectedBarcodes);

echo 'Total products: ', count($skus), PHP_EOL,
     'Total products with barcodes: ', $numberOfBarcodes, PHP_EOL,
     'Unique barcodes: ', count($uniqueBarcodes), PHP_EOL,
     'Intersected barcodes with SKUs: ', count($intersectedBarcodes), PHP_EOL;

unset($skus, $products);

//First we swap products with unique barcodes, then products with barcodes
//which intersect with SKUs of products from the first group.
//Thus we won't violate uniqness of SKU values in DB during swapping.
$groups = [$uniqueBarcodes, $intersectedBarcodes];

unset($uniqueBarcodes, $intersectedBarcodes);

foreach ($groups as $group) {
  $products = Mage::getResourceModel('catalog/product_collection')
    ->addAttributeToSelect('*')
    ->addIdFilter($group)
    ->setOrder('updated_at',  Varien_Data_Collection::SORT_ORDER_ASC);

  foreach ($products as $product) {
    $sku = $product['sku'];

    $product['sku'] = $product['product_barcode_'];
    $product['product_barcode_'] = $sku;

    $product->save();
  }
}

} catch (Exception $e) {
  Mage::printException($e);
  exit(1);
}
