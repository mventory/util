#!/bin/bash

INSTANCE_PATH=/www/dev

DIR_MODULES=modules
DIR_THEMES=themes
DIR_MAGENTO=htdocs
DIR_MODMAN=.modman

function upgrade_module {
  upgrade_repo $INSTANCE_PATH/$DIR_MODULES/$1 $2
}

function upgrade_theme {
  upgrade_repo $INSTANCE_PATH/$DIR_THEMES/$1 $2
}

function reset_main_repo {
  upgrade_repo $INSTANCE_PATH/$DIR_MAGENTO $1
}

function upgrade_repo {
  cd $1

  git fetch
  git reset --hard origin/$2
}

function upgrade_repos {
  upgrade_module Inchoo_SocialConnect tmp
  upgrade_module wap-custom-menu feature/category-custom-url
  upgrade_module mage-enhanced-admin-grids 1.0.0-wip

  upgrade_module MVentory_API develop
  upgrade_module MVentory_TradeMe chances/develop
  upgrade_module MVentory_Sync chances
  upgrade_module MVentory_Productivity master
  upgrade_module MVentory_UI/UICategoryExternalLink master
  upgrade_module MVentory_UI/UICategoriesList master
  upgrade_module MVentory_Chances master

  upgrade_theme themes2/chances.co.nz/theme master
}

function undeploy {
  cd $INSTANCE_PATH

  #It's not possible to undeploy modules installed by copying files,
  #i.e. using --copy flag. So first we convert module files into links and
  #then undeploy everything

  modman repair --force

  for module in $(modman list)
  do
    modman undeploy --copy $module
  done
}

function deploy {
  cd $INSTANCE_PATH

  for module in $(modman list)
  do
    modman deploy --copy $module
  done
}

#Configure git to remember password for 15 minutes by default
git config --global credential.helper cache

reset_main_repo development

undeploy
upgrade_repos
deploy

#Rebuild CSS
cd $INSTANCE_PATH/$DIR_MAGENTO/skin/frontend/chances/default/scss
compass clean
compass compile
