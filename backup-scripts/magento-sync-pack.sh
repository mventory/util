# This script creates an archive of an existing magento installation inside the application folder and a database backup
# A backup directory needs to be created before executing the backup script inside the applications directory
# IMPORTANT - This backup script needs to be executed using 'sudo' so that user permission and ownership remains the same
# Check magento XML configuration file for DB configurations
# Make required configurations as commented below:

#Change the name of the backup here-
BACKUP_NAME="production"

BACKUP_DIR="/www/$BACKUP_NAME/backups"
MAGENTO_DIR="/www/$BACKUP_NAME/htdocs"

#Change DB configurations here-
DB=""
DB_USER=""
DB_PASS=""

IGNORE_DIRS="
  media/catalog/product
  var/backups
  var/cache
  var/export
  var/import
  var/importexport
  var/locks
  var/log
  var/report
  var/session
"
mysqldump $DB -u $DB_USER -p$DB_PASS > $MAGENTO_DIR/$BACKUP_NAME.sql

IGNORE_DIRS_=""

for DIR in $IGNORE_DIRS
do
  IGNORE_DIRS_=$IGNORE_DIRS_" --exclude="$DIR
done

tar --create --gzip --file $BACKUP_DIR/$BACKUP_NAME.tar.gz --directory $MAGENTO_DIR --exclude-vcs $IGNORE_DIRS_ .

rm $MAGENTO_DIR/$BACKUP_NAME.sql
