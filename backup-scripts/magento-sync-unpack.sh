# This script unpacks a magento installation which has been backup using the magento-sync-unpack script.
# Make required configurations as commented below:
# IMPORTANT - This backup script needs to be executed using 'sudo' so that user permission and ownership remains the same

# Change name of backup to be restored-
BACKUP_NAME="chambers"

BACKUP_DIR="/web/apps/backup"
MAGENTO_DIR="/web/apps/$BACKUP_NAME/htdocs"

#Change DB configurations here-
DB="chambers_magento"
DB_USER="chambers_magento"
DB_PASS="chambers"

IGNORE_DIRS="
  media
  var/cache
  var/export
  var/import
  var/locks
  var/log
  var/report
"

mysql $DB -u $DB_USER -p$DB_PASS < $BACKUP_DIR/db_backup_$BACKUP_NAME.sql

cd $BACKUP_DIR

tar -xf $BACKUP_NAME.tar.gz --directory $MAGENTO_DIR --same-permissions
