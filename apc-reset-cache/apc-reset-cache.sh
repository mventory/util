#!/bin/bash

# This bash script creates a simple PHP script to clear the APC cache
# in the root directory of the website and calls it via HTTP to
# execute it in the same environment as PHP code of the website.

# Replace with your local path and URL
DIR_ROOT='/var/www/htdocs'
URL_ROOT='http://www.foobar.com'

# The PHP script is given a random name
random_name=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
script_name=$random_name.php

# Write the contents of the PHP script into a file
cat > $DIR_ROOT/$script_name<<EOF
<?php

echo apc_clear_cache() && apc_clear_cache('user');
EOF

# Execute and delete the PHP script
result=$(wget --quiet -O - $URL_ROOT/$script_name)
rm $DIR_ROOT/$script_name

# Check and output the result
if [[ $result == 1 ]]
then
  echo 'Successfully reset APC cache'
else
  echo 'Failed to reset APC cache'
  echo $result
fi
