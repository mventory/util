#!/bin/bash

DIR_BASE='/var/www/html'
USER='ubuntu'
GROUP='www-data'

if [ $(id -u) != 0 ]; then
  echo 'This script should be run as root so that file ownership changes can be set correctly'
  exit
fi

for site_path in $DIR_BASE/*;
do
  #Check if node is directory and is wordpress directory
  [[ -d $site_path && -f $site_path/wp-config.php ]] || continue

  chown -R $USER:$GROUP $site_path

  #By default web-server can't change wordpress files
  find $site_path -type f -exec chmod 644 {} \;
  find $site_path -type d -exec chmod 755 {} \;

  #Make everything in wp-content writable by web user except ...
  find $site_path/wp-content -type f -exec chmod 664 {} \;
  find $site_path/wp-content -type d -exec chmod 775 {} \;

  #source code for themes ...
  find $site_path/wp-content/themes -type f -exec chmod 644 {} \;
  find $site_path/wp-content/themes -type d -exec chmod 755 {} \;

  #and plugins
  find $site_path/wp-content/plugins -type f -exec chmod 644 {} \;
  find $site_path/wp-content/plugins -type d -exec chmod 755 {} \;
done
