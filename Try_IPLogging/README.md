Try_IPLogging
=============

An utility extension to log IP address of API users when they create
products via the app.

IP address is stored in `api_request` attribute. The attribute should be
created prior using the extension with _Text Field_ or _Text Area_ input type.
