<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Creative Commons License BY-NC-ND.
 * NonCommercial — You may not use the material for commercial purposes.
 * NoDerivatives — If you remix, transform, or build upon the material,
 * you may not distribute the modified material.
 * See the full license at http://creativecommons.org/licenses/by-nc-nd/4.0/
 *
 * See http://mventory.com/legal/licensing/ for other licensing options.
 *
 * @package Try/IPLogging
 * @copyright Copyright (c) 2015 mVentory Ltd. (http://mventory.com)
 * @license http://creativecommons.org/licenses/by-nc-nd/4.0/
 */

/**
 * Event observers
 *
 * @package Try/IPLogging
 * @author Anatoly A. Kazantsev <anatoly@mventory.com>
 */

class Try_IPLogging_Model_Observer
{
  /**
   * Log API request data in product attribute
   *
   * @param Varien_Event_Observer $observer
   *   Observer object
   *
   * @return Try_IPLogging_Model_Observer
   *   Instance of this class
   */
  public function log ($observer) {
    $observer
      ->getProduct()
      ->setData(
          'api_request',
          'Remote IP: ' . Mage::helper('core/http')->getRemoteAddr()
        );

    return $this;
  }
}