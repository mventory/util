# magento_permissions

This script sets appropriate permissions on all files inside Magento root and
it's subdirectories.

Must be executed using sudo in Magento root directory.
