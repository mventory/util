#!/bin/bash

#The script requires root, sun it with sudo
#It accepts and requires 3 parameters:
#  1. app name
#  2. app domain name (without http://, without www and without trailing /, e.g. example.com)
#  3. Path to magento archive

set -e  # exit on any error (non-zero value)
set -u  # treat reference to an undefined variable as error

if [[ $# -ne 3 ]]; then
    echo "Illegal number of parameters"
    exit 255
fi

if [[ $(id -u) -ne 0 ]]; then
    echo "Please run as root"
    exit 254
fi

MYSQL_CONNECT="--defaults-extra-file=/etc/offsider-cnf.txt"
DIR_APPS="/web/apps"

app_name=$1
app_domain=$2
magento_archive=$3

db=$app_name
db_user=$app_name
db_password=$app_name

app_dir=$DIR_APPS/$app_name
conf_dir=$app_dir/conf
htdocs_dir=$app_dir/htdocs
certs_dir=$conf_dir/certs
fpm_dir=$conf_dir/php-fpm

mkdir $app_dir
chown bitnami:bitnami $app_dir
chmod 755 $app_dir

mkdir $conf_dir
chown bitnami:bitnami $conf_dir
chmod 755 $conf_dir

mkdir $htdocs_dir
chown bitnami:daemon $htdocs_dir
chmod 2755 $htdocs_dir

mkdir $certs_dir
chown bitnami:bitnami $certs_dir
chmod 755 $certs_dir

mkdir $fpm_dir
chown bitnami:bitnami $fpm_dir
chmod 755 $fpm_dir

cat >$certs_dir/server.crt <<EOF
-----BEGIN CERTIFICATE-----
MIICqDCCAZACCQCz8T3726LYsjANBgkqhkiG9w0BAQUFADAWMRQwEgYDVQQDDAtl
eGFtcGxlLmNvbTAeFw0xMjExMTQxMTE4MjdaFw0yMjExMTIxMTE4MjdaMBYxFDAS
BgNVBAMMC2V4YW1wbGUuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKC
AQEA5NHl5TfZtO6zugau2tp5mWIcQYJhuwKTmYeXDLYAGJpoD2SixwPL5c8glneI
Rz1N2EQIZVeaWGbS0FLFlPdOkCkplpW9isYVC4XqKrk5b4HW4+YC+Cup0k+Kd4NM
eZOTUvWr5N6dIpdibkVumBc/pao8VtdwywlCL/PwGRsQtkXrRICzdtRa3MXqTmEF
foyVCGgBRtronlB9x4Plfb8Psk4GrPkjrWYgO8peKrl0O5+F+sYg7Gj95zCH73BQ
ANzCVNrgD9fs9cyx3ru9CUdEoIxAAJwQFkjm7xr6xqhIlSgnQ7B0uOSTNRcXY6rw
s+PxGneec/kRPRgzjC/QHY6n8QIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQBbyMqF
RDsX8zX1EW5qA8AQ8Jb2XqWrVeSO8blMV3WagJ2airMm3+c/82FCwsd/cZ08UXhA
/Kou0gi/F16tV26PiiUdp590Qao3d8H2qxc1rzzULimZPgxH4iA4vRyMHtyZN6h4
7Fdn7O9xNMPu8siOz8rrzsEdEX5URbOMkDLCZsbTIUWVv2XmqrR0K10d5VuLWeLi
r+4G6c6jpa244WmqT9ClqceJ12G1Wnmezy7ybiW0l5M2iuIKFEiRP5Hj0J15o1I2
pXAbKysAdWRHsJSQOtcgO8Vh9k0wo3tKg4HDp1hbrEzoGzOv92Vjg3lG8X+hzbMJ
MQURotHkD4Gk57wL
-----END CERTIFICATE-----
EOF

chown bitnami:bitnami $certs_dir/server.crt
chmod 644 $certs_dir/server.crt


cat >$certs_dir/server.key <<EOF
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEA5NHl5TfZtO6zugau2tp5mWIcQYJhuwKTmYeXDLYAGJpoD2Si
xwPL5c8glneIRz1N2EQIZVeaWGbS0FLFlPdOkCkplpW9isYVC4XqKrk5b4HW4+YC
+Cup0k+Kd4NMeZOTUvWr5N6dIpdibkVumBc/pao8VtdwywlCL/PwGRsQtkXrRICz
dtRa3MXqTmEFfoyVCGgBRtronlB9x4Plfb8Psk4GrPkjrWYgO8peKrl0O5+F+sYg
7Gj95zCH73BQANzCVNrgD9fs9cyx3ru9CUdEoIxAAJwQFkjm7xr6xqhIlSgnQ7B0
uOSTNRcXY6rws+PxGneec/kRPRgzjC/QHY6n8QIDAQABAoIBACo3G131tuGtpFTu
xLW11vdYZXQklNlGuWp63IBI162yVv54B5wF9Ek6tH1uIiNaiREcRBxGVEB4/+3V
R4SbN9Ba98RDbgu7TcipdTFaqOEMqFO1bNjSXWtip14zSBmqA2Ur1AHOnFj0awGD
J8tBhsmOpcEz0Ch1VdO5ApPvLV8jH9wQiMI/Q6yYQMtmzTMCUMYdMqe+LOziIOzL
oqN/WXnKL5E5TiO1bIxSpWPbT+IVn1c3/PShmvmRrLWsFUQlkwXJKMYZPO+rCCfe
b+Q9lMLMnj+vOnM3z16WC3aiiJGCZjVTvQ+x22YrBTRPxZmHO2eZ4H/cUQM7Y/tw
I7RjEM0CgYEA9Kxt1t8bWonzBii3P0rwyx0IECvg63k+pp4BpxpeWQKL7NVdSzk3
AyJVcNjUoZgi2kVPdxzZGLrnZfuZ691xQB3oZF0LwBzQ4GFHkTRCB0s8ZA5lcJaI
9pBu91bhz2VOZSTeQWpdMMURjXVyTXZInU1mwzmjVOIAYmO33shH9gcCgYEA72mX
UoIrFPLkOTSZOb7UbjYH01vf6ThQiYCEWg7mD3CbY7n9oobIcQMzNnt7xN4wOl/V
eKfZ7G56q8enfqm45Dyo9aCBCENVzmwO8wLe5UnvJBNL20KjvtwG8w5A6UZQzC7p
3QS+U2zxVQNEeaE6a8Wrq2d1PlhVAHYw8odgNEcCgYBN38+58xrmrz99d1oTuAt5
6kyVsRGOgPGS4HmQMRFUbT4R7DscZSKASd4945WRtTVqmWLYe4MRnvNlfzYXX0zb
ZmmAAClsRP+qWuwHaEWXwrd+9SIOOqtvJrta1/lZJFpWUOy4j10H18Flb7sosnwc
LPWHL4Iv0xriNfDg5Iga4wKBgQDLJBU59SkJBW+Q+oho7vrg6QeK15IOGbJ8eYfT
woCC6VFwNQh5N1QsUELMH8rNKJpTba18SzAl5ThBOY9tciVnw/C5Og9CK6BLHnUw
zWbDtxAq1BSxXsIB2EAtTBLX3MoB9myJFNVJhE7hi3w2mA8yEu+u6IIa/Ghjk+XE
ZAnFUQKBgQDjMinRZrK5wA09jcetI+dNiLnKHoQG6OaXDDsNCatex0O2F36BvVXE
P78qDz/i5aBMWsLx6VDvWJAkBIpZoNS5UsOn17tFaocGUSkcm48bs8Dn6VvsE8Bd
XMPAHyKuILlKYifBvNq5T22KhqKX7yGmk/AeOOiKr2KeMnh27JYrCA==
-----END RSA PRIVATE KEY-----
EOF

chown bitnami:bitnami $certs_dir/server.key
chmod 644 $certs_dir/server.key

cat >$fpm_dir/pool.conf <<EOF
[${app_name}]
listen=/opt/bitnami/php/var/run/${app_name}.sock
include=/opt/bitnami/php/etc/common-ondemand.conf
include=${fpm_dir}/php-settings.conf
include=/opt/bitnami/php/etc/environment.conf
pm=ondemand
pm.max_requests = 500
EOF

chown bitnami:bitnami $fpm_dir/pool.conf
chmod 644 $fpm_dir/pool.conf

cat >$fpm_dir/php-settings.conf <<EOF
php_flag[zend.ze1_compatibility_mode]=off

php_flag[suhosin.session.cryptua]=off
php_flag[session.auto_start]=off
php_flag[magic_quotes_gpc]=off
php_value[max_execution_time]=18000
php_value[memory_limit]=256M
php_flag[engine]=off
EOF

chown bitnami:bitnami $fpm_dir/php-settings.conf
chmod 644 $fpm_dir/php-settings.conf

cat >$conf_dir/htaccess.conf <<EOF
<Directory "${htdocs_dir}">
############################################
## uncomment these lines for CGI mode
## make sure to specify the correct cgi php binary file name
## it might be /cgi-bin/php-cgi

#    Action php5-cgi /cgi-bin/php5-cgi
#    AddHandler php5-cgi .php

############################################
## GoDaddy specific options

#  Options -MultiViews

## you might also need to add this line to php.ini
##     cgi.fix_pathinfo = 1
## if it still doesn't work, rename php.ini to php5.ini

############################################
## this line is specific for 1and1 hosting

    #AddType x-mapp-php5 .php
    #AddHandler x-mapp-php5 .php

############################################
## default index file

    DirectoryIndex index.php

<IfModule mod_php5.c>

############################################
## adjust memory limit

#    php_value memory_limit 64M
    php_value memory_limit 256M
    php_value max_execution_time 18000

############################################
## disable magic quotes for php request vars

php_flag magic_quotes_gpc off

############################################
## disable automatic session start
## before autoload was initialized

    php_flag session.auto_start off

############################################
## enable resulting html compression

    #php_flag zlib.output_compression on

###########################################
# disable user agent verification to not break multiple image upload

    php_flag suhosin.session.cryptua off

###########################################
# turn off compatibility with PHP4 when dealing with objects

    php_flag zend.ze1_compatibility_mode Off

</IfModule>

<IfModule mod_security.c>
###########################################
# disable POST processing to not break multiple image upload

    SecFilterEngine Off
    SecFilterScanPOST Off
</IfModule>

<IfModule mod_deflate.c>

############################################
## enable apache served files compression
## http://developer.yahoo.com/performance/rules.html#gzip

    # Insert filter on all content
    ###SetOutputFilter DEFLATE
    # Insert filter on selected content types only
    #AddOutputFilterByType DEFLATE text/html text/plain text/xml text/css text/javascript

    # Netscape 4.x has some problems...
    #BrowserMatch ^Mozilla/4 gzip-only-text/html

    # Netscape 4.06-4.08 have some more problems
    #BrowserMatch ^Mozilla/4\.0[678] no-gzip

    # MSIE masquerades as Netscape, but it is fine
    #BrowserMatch \bMSIE !no-gzip !gzip-only-text/html

    # Don't compress images
    #SetEnvIfNoCase Request_URI \.(?:gif|jpe?g|png)$ no-gzip dont-vary

    # Make sure proxies don't deliver the wrong content
    #Header append Vary User-Agent env=!dont-vary

</IfModule>

<IfModule mod_ssl.c>

############################################
## make HTTPS env vars available for CGI mode

    SSLOptions StdEnvVars

</IfModule>

<IfModule mod_rewrite.c>

############################################
## enable rewrites

    Options +FollowSymLinks
    RewriteEngine on

############################################
## you can put here your magento root folder
## path relative to web root

    #RewriteBase /magento/

############################################
## uncomment next line to enable light API calls processing

#    RewriteRule ^api/([a-z][0-9a-z_]+)/?$ api.php?type=$1 [QSA,L]

############################################
## rewrite API2 calls to api.php (by now it is REST only)

    RewriteRule ^api/rest api.php?type=rest [QSA,L]

############################################
## workaround for HTTP authorization
## in CGI environment

    RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

############################################
## TRACE and TRACK HTTP methods disabled to prevent XSS attacks

    RewriteCond %{REQUEST_METHOD} ^TRAC[EK]
    RewriteRule .* - [L,R=405]

############################################
## redirect for mobile user agents

    #RewriteCond %{REQUEST_URI} !^/mobiledirectoryhere/.*$
    #RewriteCond %{HTTP_USER_AGENT} "android|blackberry|ipad|iphone|ipod|iemobile|opera mobile|palmos|webos|googlebot-mobile" [NC]
    #RewriteRule ^(.*)$ /mobiledirectoryhere/ [L,R=302]

############################################
## always send 404 on missing files in these folders

    RewriteCond %{REQUEST_URI} !^/(media|skin|js)/

############################################
## never rewrite for existing files, directories and links

    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-l

############################################
## rewrite everything else to index.php

    RewriteRule .* index.php [L]

</IfModule>


############################################
## Prevent character encoding issues from server overrides
## If you still have problems, use the second line instead

    AddDefaultCharset Off
    #AddDefaultCharset UTF-8

<IfModule mod_expires.c>

############################################
## Add default Expires header
## http://developer.yahoo.com/performance/rules.html#expires

    ExpiresDefault "access plus 1 year"

</IfModule>

############################################
## By default allow all access

    Order allow,deny
    Allow from all

###########################################
## Deny access to release notes to prevent disclosure of the installed Magento version

    <Files RELEASE_NOTES.txt>
        order allow,deny
        deny from all
    </Files>

############################################
## If running in cluster environment, uncomment this
## http://developer.yahoo.com/performance/rules.html#etags

    #FileETag none

</Directory>
<Directory "${htdocs_dir}/app">
Order deny,allow
Deny from all

</Directory>
<Directory "${htdocs_dir}/downloader">
<IfModule mod_deflate.c>

    RemoveOutputFilter DEFLATE
    RemoveOutputFilter GZIP

</IfModule>

<Files ~ "\.(cfg|ini|xml)$">
    order allow,deny
    deny from all
</Files>
</Directory>
<Directory "${htdocs_dir}/downloader/template">
Order deny,allow
Deny from all

</Directory>
<Directory "${htdocs_dir}/errors">
<FilesMatch "\.(xml|phtml)$">
    Deny from all
</FilesMatch>
</Directory>
<Directory "${htdocs_dir}/includes">
Order deny,allow
Deny from all

</Directory>
<Directory "${htdocs_dir}/lib">
Order deny,allow
Deny from all

</Directory>
<Directory "${htdocs_dir}/media">
Options All -Indexes
<IfModule mod_php5.c>
php_flag engine 0
</IfModule>

AddHandler cgi-script .php .pl .py .jsp .asp .htm .shtml .sh .cgi
Options -ExecCGI

<IfModule mod_rewrite.c>

############################################
## enable rewrites

    Options +FollowSymLinks
    RewriteEngine on

############################################
## never rewrite for existing files
    RewriteCond %{REQUEST_FILENAME} !-f

############################################
## rewrite everything else to index.php

    RewriteRule .* ../get.php [L]
</IfModule>

</Directory>
<Directory "${htdocs_dir}/media/customer">
Order deny,allow
Deny from all

</Directory>
<Directory "${htdocs_dir}/media/downloadable">
Order deny,allow
Deny from all

</Directory>
<Directory "${htdocs_dir}/pkginfo">
Order deny,allow
Deny from all

</Directory>
<Directory "${htdocs_dir}/skin/frontend/rwd/default/scss">
Order deny,allow
Deny from all

</Directory>
<Directory "${htdocs_dir}/var">
Order deny,allow
Deny from all

</Directory>
EOF

chown bitnami:bitnami $conf_dir/htaccess.conf
chmod 644 $conf_dir/htaccess.conf

cat >$conf_dir/httpd-vhosts.conf <<EOF
<VirtualHost *:80>
  ServerName www.${app_domain}
  DocumentRoot "${htdocs_dir}"
  Include "${conf_dir}/httpd-app.conf"
</VirtualHost>

<VirtualHost *:80>
  ServerName ${app_domain}
  Redirect permanent / http://www.${app_domain}/
</VirtualHost>

<VirtualHost *:443>
  ServerName www.${app_domain}
  DocumentRoot "${htdocs_dir}"
  SSLEngine on
  SSLCertificateFile "${certs_dir}/server.crt"
  SSLCertificateKeyFile "${certs_dir}/server.key"
  Include "${conf_dir}/httpd-app.conf"
</VirtualHost>

<VirtualHost *:443>
  ServerName ${app_domain}
  Redirect permanent / https://www.${app_domain}/
</VirtualHost>
EOF

chown bitnami:bitnami $conf_dir/httpd-vhosts.conf
chmod 644 $conf_dir/httpd-vhosts.conf

cat >$conf_dir/httpd-prefix.conf <<EOF
# App url moved to root
DocumentRoot "${htdocs_dir}"
Include "${conf_dir}/httpd-app.conf"
EOF

chown bitnami:bitnami $conf_dir/httpd-prefix.conf
chmod 644 $conf_dir/httpd-prefix.conf

cat >$conf_dir/httpd-app.conf <<EOF
<IfDefine USE_PHP_FPM>
    <Proxy "unix:/opt/bitnami/php/var/run/${app_name}.sock|fcgi://${app_name}-fpm/" timeout=300>
    </Proxy>
</IfDefine>

<Directory "${htdocs_dir}">
    Options -MultiViews
    AllowOverride None

    <IfVersion < 2.3 >
    Order allow,deny
    Allow from all
    </IfVersion>
    <IfVersion >= 2.3>
    Require all granted
    </IfVersion>

    <IfDefine USE_PHP_FPM>
       <FilesMatch \.php$>
         SetHandler "proxy:fcgi://${app_name}-fpm/"
       </FilesMatch>
    </IfDefine>
</Directory>

Include "${conf_dir}/htaccess.conf"
EOF

chown bitnami:bitnami $conf_dir/httpd-app.conf
chmod 644 $conf_dir/httpd-app.conf

echo "Include \"${conf_dir}/httpd-vhosts.conf\"" >> /opt/bitnami/apache2/conf/bitnami/bitnami-apps-vhosts.conf
echo "include=${fpm_dir}/pool.conf" >> /opt/bitnami/php/etc/php-fpm.conf

/opt/bitnami/ctlscript.sh restart php-fpm
/opt/bitnami/ctlscript.sh restart apache

mysql $MYSQL_CONNECT --execute "CREATE DATABASE ${db} CHARACTER SET 'utf8'; CREATE USER '${db_user}'@'localhost' IDENTIFIED BY '${db_password}'; GRANT ALL PRIVILEGES ON \`${db}\`.* TO '${db_user}'@'localhost'; FLUSH PRIVILEGES;"

cd $htdocs_dir
tar -xf $magento_archive

touch $htdocs_dir/app/etc/local.xml    # file needs to exist for permissions.sh
chown -R bitnami:daemon ${htdocs_dir}
mkdir $htdocs_dir/feeds
/home/bitnami/bin/permissions.sh
rm $htdocs_dir/app/etc/local.xml

php -f install.php -- --license_agreement_accepted yes \
    --locale en_NZ --timezone "Pacific/Auckland" --default_currency NZD \
    --db_host localhost --db_name $db --db_user $db_user --db_pass $db_password \
    --url "http://${app_domain}/" --admin_frontname "mvadmin" --use_rewrites yes \
    --use_secure no --secure_base_url "http://${app_domain}/" --use_secure_admin no \
    --admin_lastname mVentory --admin_firstname Admin --admin_email "support@mventory.com" \
    --admin_username mvadmin --admin_password mvadminmvadmin1 --skip_url_validation

mysql $MYSQL_CONNECT -e "DELETE FROM ${db}.adminnotification_inbox;"  >/dev/null 2>&1

echo "All Done."

