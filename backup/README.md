# Backup

To run simply execute backup.sh manually, or via cron (as bitnami user):

`bacup.sh /path/to/roles.txt [days]`

The script takes a collection of roles (in arn:: form) and regions, stored in roles file  and creates snapshots of all volumes accessible under a given role in a given region with a tag "Backup" set to "true" (case sensitive).

Roles file should contain one role/region pair per line, e.g.

arn:aws:iam::447056982714:role/mvbackup ap-southeast-2  # client X <BR>
arn:aws:iam::914158972811:role/mvbackup ap-southeast-2  # client Y <BR>
us-east-1 # if role is not specified, our account will be backed-up in this region

All output is written to `backup.log` file, currently located in `/home/bitnami/backup/`.


The script uses a modified version of `ec2-automate-backup-awscli.sh` from [AWS Missing Tools](https://github.com/colinbjohnson/aws-missing-tools/tree/master/ec2-automate-backup).

The script will create snapshots with the following name: {volume name}_{date} <BR>
If the script is given a second optional argument `days` (number of days to keep the snapshots), it will add the following tags: <BR>
PurgeAllow: true <BR>
PurgeAfterFE: date (in seconds since epoch) after which the snapshot can be purged

All snapshots with PurgeAfterFE < current date will be purged on next script execution.

See [AWS Missing Tools](https://github.com/colinbjohnson/aws-missing-tools/tree/master/ec2-automate-backup) for a more detailed description.

*NOTE:* AWS CLI tools must be installed and on the path.
