#!/bin/bash

# backup.sh /path/to/roles.txt [days]
# second argument is optional, but if provided, but be numeric,
# specifying number of days a snapshot should be kept.

# Take arn role as an argument and assumes the role.
assume_role() {
  sts_assume_role=$(aws sts assume-role \
                  --role-arn $1 \
                  --role-session-name mvbackup --output text 2>&1) 
  #takes the output of the previous command 
  sts_assume_role_result=$(echo $?)
  if [[ $sts_assume_role_result -gt 0 ]]; then
    echo -e "An error occurred when running sts-assume-role on $1. 
The error returned is below:\nCode: $sts_assume_role_result
Message: $sts_assume_role" >> $logfile ; return 90
  fi
  role=$(echo "$sts_assume_role" | grep ^CREDENTIALS ) 
  export AWS_ACCESS_KEY_ID=$(echo "$role" | cut -f 2)
  export AWS_SECRET_ACCESS_KEY=$(echo "$role" | cut -f 4)
  export AWS_SESSION_TOKEN=$(echo "$role" | cut -f 5)

}

logfile="/home/bitnami/backup/backup.log"
echo "--------------------------------------------
Backup started at $(date -u)
-------------------------------------------" >> $logfile

if [[ $# -eq 0 ]]; then
  echo "Error: You must provide a path to roles file. Terminating." >> $logfile
  exit 100
fi

if [ ! -f $1 ]; then
  echo "Error: roles file $1 does not exist. Terminating." >> $logfile
  exit 100
else
  echo "Using roles from $1." >> $logfile
  filename=$1
fi

numericReg='^[0-9]+$'
if [[ $2 =~ $numericReg ]]; then
  purgeAfterOpt="-k $2";  # add purgeAfterFE option to ec2-automate-backup-awscli.sh
fi

regex="(.*)[#|$]"  # regex to remove comments from end of the line
while read -r line || [[ -n $line ]]  # || ...  part is in case last line has no EOL
do

  if [[ $line =~ $regex ]]; then
    line="${BASH_REMATCH[1]}"  # remove any comments from the end of the line
  fi

  read -a array <<< "$line" # split line into array

  if [[ "${#array[@]}" -eq 0 ]]; then continue; fi

  if [[ "${#array[@]}" -eq 1 ]]; then
    region=${array[0]}  # if no role specified, first element is the region
  else
    region=${array[1]}
    if assume_role ${array[0]}; then
        echo "Role ${array[0]} assumed successfully." >> $logfile
    else
        echo "Skipping ${array[0]}" >> $logfile
    fi
  fi

  result=$(ec2-automate-backup-awscli.sh -s tag -t "Backup,Values=true" -r $region -n $purgeAfterOpt -p >> $logfile 2>&1)
  
  unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN

done < "$filename"

echo "Backup finished at $(date -u)" >> $logfile
