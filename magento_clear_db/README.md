# clear_db.php - removes all table from a specified DB

Useful for hosted environments that do not allow DELETE/CREATE operations.
