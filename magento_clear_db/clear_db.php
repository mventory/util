<?php
$mysqli = new mysqli("localhost", "username", "password", "database");
$mysqli->query('SET foreign_key_checks = 0');
if ($result = $mysqli->query("SHOW TABLES"))
{
        while($row = $result->fetch_array(MYSQLI_NUM))
                {
                            $mysqli->query('DROP TABLE IF EXISTS '.$row[0]);
                                }
}

if ($result = $mysqli->query("SHOW PROCEDURE STATUS WHERE Db = DATABASE()"))
{
        while($row = $result->fetch_array(MYSQLI_NUM))
                {
                            $mysqli->query('DROP PROCEDURE IF EXISTS '.$row[1]);
                                }
}
$mysqli->query('SET foreign_key_checks = 1');
$mysqli->close();
