<?php

/**
 * @package MVentory/ProductFeed
 * @copyright Copyright (c) 2015 mVentory Ltd. (http://mventory.com)
 * @author Anatoly A. Kazantsev <anatoly@mventory.com>
 */

/**
 * Product collection
 *
 * @package MVentory/ProductFeed
 * @author Anatoly A. Kazantsev <anatoly@mventory.com>
 */
class MVentory_ProductFeed_Model_Resource_Product_Collection
  extends Mage_Catalog_Model_Resource_Product_Collection
{
  /**
   * Add filtering by specified stock statuse
   *
   * @param int $modes
   *   Stock status
   *
   * @return MVentory_ProductFeed_Model_Resource_Product_Collection
   *   Instance of this class
   */
  public function addStockStatusFilter ($status) {
    $modes = $this->_getStockModes($status);

    if (!$conds = $this->_convertModesToConds($modes))
      return $this;

    $this->joinField(
      'inventory_manage_stock',
      'cataloginventory/stock_item',
      'manage_stock',
      'product_id=entity_id',
      $conds['status']
    );

    $this->joinField(
      'inventory_qty',
      'cataloginventory/stock_item',
      'qty',
      'product_id=entity_id',
      isset($conds['qty']) ? $conds['qty'] : null
    );

    return $this;
  }

  /**
   * Convert supplied stock mode to list of stock modes
   *
   * @param int $status
   *   Stock status
   *
   * @return array
   *   List of modes for stock filtering
   *
   * @throws UnexpectedValueException
   *   If supplied stock status is unknown
   */
  protected function _getStockModes ($status) {
    switch ($status) {
      case MVentory_ProductFeed_Model_Feed::STOCK_NOT_MANAGED:
        return [['managed' => 0]];
      case MVentory_ProductFeed_Model_Feed::STOCK_IN:
        return [['managed' => 1, 'stock' => 1]];
      case MVentory_ProductFeed_Model_Feed::STOCK_BOTH:
        return [
          ['managed' => 0],
          ['managed' => 1, 'stock' => 1]
        ];
    }

    throw new UnexpectedValueException('Unexpected stock status');
  }

  /**
   * Convert list of modes for stock filtering into SQL conditions to apply
   * to product collection
   *
   * @param array $modes
   *   List of modes for stock filtering
   *
   * @return array
   *   List of prepared SQL conditions to filter product collection
   */
  protected function _convertModesToConds ($modes) {
    $storeManaged = (int) Mage::app()
      ->getStore($this->getStoreId())
      ->getConfig(
          Mage_CatalogInventory_Model_Stock_Item::XML_PATH_MANAGE_STOCK
        );

    foreach ($modes as $mode) {
      $andCond = [];

      $andCond[] = '{{table}}.use_config_manage_stock = 0';
      $andCond[] = '{{table}}.manage_stock = ' . $mode['managed'];

      if (isset($mode['stock']))
        $andCond[] = '{{table}}.is_in_stock = ' . $mode['stock'];

      $cond[] = implode(' AND ', $andCond);

      if ($mode['managed'] == $storeManaged) {
        $andCond = [];

        $andCond[] = '{{table}}.use_config_manage_stock = 1';

        if (isset($mode['stock'])) {
          $andCond[] = '{{table}}.is_in_stock = ' . $mode['stock'];

          if (isset($mode['min_qty']))
            $qtyCond = '{{table}}.qty >= ' . $mode['min_qty'];
        }

        $cond[] = implode(' AND ', $andCond);
      }
    }

    $conds = [
      'status' => isset($cond) ? '(' . implode(') OR (', $cond) . ')' : null
    ];

    if (isset($qtyCond))
      $conds['qty'] = $qtyCond;

    return $conds;
  }
}
