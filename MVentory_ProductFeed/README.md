MVentory_ProductFeed
=============

A simple extension to generate product feeds for PriceMe and PriceSpy services.

Conditions for being included in a feed:

* Status: enabled
* Visibility: catalog, search
* Include in product feeds attribute: Yes
* Stock: in stock and/or unmanaged (configurable)

_Include in product feeds_ (`productfeed_include`) attribute is created and added to all existing attribute sets during installation of the extension. By default products are not included into feeds. Set this attribute's value to Yes for any products to be included in the feeds (the above conditions still need to be met for the product to be included).

Products can be filtered by stock status, categories and attribute sets. Only products with `productfeed_include` = yes will be included, so check this for all products before doing filtering by category or attribute set (again above conditions must also be met).

Available values for filtering by stock status:

* MVentory_ProductFeed_Model_Feed::STOCK_IN - Managed stock and in stock (default)
* MVentory_ProductFeed_Model_Feed::STOCK_NOT_MANAGED - Stock not managed
* MVentory_ProductFeed_Model_Feed::STOCK_BOTH - Both of the above


Example of filtering products:
```php
#File: app/code/community/MVentory/ProductFeed/Model/Observer.php

$feed->generate(array(
  //...

  'stock_status' => MVentory_ProductFeed_Model_Feed::STOCK_BOTH,
  'categories' => array(5, 6),
  'attribute_sets' => array(4, 10),

  //...
));
```

Only products meeting the above conditions will be included in the feed.

Feeds are generated at 2am everyday and are placed in `feeds/` directory.
