#!/bin/bash
# This script sets secure and unsecure web URLs.
# Usage:
#       set_base_url.sh http://example.com/
# Make sure URL is preceeded by the protocol (http or https) and
# followed by a /
#
# andrew@mventory.com
# 14 September 2015 

if [ ! -f ./app/etc/local.xml ]; then
    echo "-- ERROR"
    echo "-- This doesn't look like a Magento install.  Please make sure"
    echo "-- that you are running this from the Magento main doc root dir"
    exit
fi
if [ $# -ne 1 ]; then
    echo "-- ERROR"
    echo "-- This script should be run with one argument - the new URL:"
    echo "-- $0 http://example.com/"
    echo "-- URL must be preceeded by either http or https and end with /"
    exit
fi
# using xpath for parsing local.xml requires libxml-xpath-perl to be installed:
# sudo apt-get install libxml-xpath-perl
host=$(xpath  -e "/config/global/resources/default_setup/connection/host/text()" ./app/etc/local.xml 2>/dev/null)
db=$(xpath  -e "/config/global/resources/default_setup/connection/dbname/text()" ./app/etc/local.xml 2>/dev/null)
user=$(xpath  -e "/config/global/resources/default_setup/connection/username/text()" ./app/etc/local.xml 2>/dev/null)
password=$(xpath  -e "/config/global/resources/default_setup/connection/password/text()" ./app/etc/local.xml 2>/dev/null)
echo "Current values of web/secure and web/unsecure URLs in database $db are:"
mysql -h $host -u $user -p$password $db -e "select path, value from core_config_data where path like 'web/%secure/base_url';"
read -p "Would you like to change these to $1 (y/n): "  -n 1 -r
echo ""
if [[ $REPLY =~ ^[Yy]$ ]]
then
    mysql -h $host -u $user -p$password $db -vv -e "update core_config_data set value = '$1' where path = 'web/unsecure/base_url'; update core_config_data set value = '$1' where path = 'web/secure/base_url';"
fi
