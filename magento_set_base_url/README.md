# set_base_url

This script sets secure and unsecure base URLs.

### Usage:
       set_base_url.sh http://example.com/

Make sure URL is preceeded by the protocol (http or https) and followed by a /
