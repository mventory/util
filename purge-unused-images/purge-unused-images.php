<?php

/**
 * This script purges image files which are no longer used in magento products
 * It doesn't remove files from cache directory. Flush catalogue images cache
 * via cache management in Magento admin.
 */

//Change current directory to the directory of current script
chdir(dirname(__FILE__));

require 'app/Mage.php';

if (!Mage::isInstalled()) {
  echo 'Application is not installed yet, please complete install wizard first.';
  exit;
}

//Only for urls
//Don't remove this
$_SERVER['SCRIPT_NAME'] = str_replace(
  basename(__FILE__),
  'index.php',
  $_SERVER['SCRIPT_NAME']
);

$_SERVER['SCRIPT_FILENAME'] = str_replace(
  basename(__FILE__),
  'index.php',
  $_SERVER['SCRIPT_FILENAME']
);

Mage::app('admin')->setUseSessionInUrl(false);

umask(0);

try {

$media = Mage::getBaseDir('media') . '/catalog/product';

$images = Mage::getSingleton('core/resource')
  ->getConnection('core_read')
  ->fetchCol('SELECT value FROM catalog_product_entity_media_gallery');

if (!count($images))
  exit('No images in DB');

$images = array_flip($images);
$files = findFiles($media, array('jpg', 'png', 'gif'));

$removedCount = 0;
$activeCount = 0;
$nonexistingCount = 0;

$activeFiles = array();

foreach ($files as $file) {
  $_file = str_replace($media, '', $file);

  if (isset($images[$_file])) {
    $activeCount++;
    $activeFiles[] = $_file;
    continue;
  }

  unlink($file);

  $removedCount++;
}

$files = array_flip($files);

foreach ($images as $img => $none)
  if (!isset($files[$media . $img]))
    $nonexistingCount++;

echo 'Number of images in DB: ', count($images), PHP_EOL,
     'Number of images in media dir: ', count($files), PHP_EOL,
     'Number of active images: ', $activeCount, PHP_EOL,
     'Number of non-exisiting images: ', $nonexistingCount, PHP_EOL,
     'Number of removed images: ', $removedCount, PHP_EOL;
} catch (Exception $e) {
  Mage::printException($e);
  exit(1);
}

function findFiles ($path, $exts = array()) {
  globRecursive(
    $path,
    array($path . DS . 'cache' => true),
    $dirs
  );

  $files = array ();

  foreach ($dirs as $dir)
    foreach ($exts as $ext)
      $files = array_merge($files, glob($dir . '/*.' . $ext));

  return $files;
}

function globRecursive ($path, $ignore = array(), &$dirs = array()) {
  foreach (glob($path, GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
    //Ignore directories
    if (isset($ignore[$dir]))
      continue;

    $dirs[] = $dir;

    globRecursive($dir . '/*', $ignore, $dirs);
  }
}