Purge unused image files
=========================

Magento doesn't remove image files if image (or whole product) was removed
via admin or programmatically using standard functionality. This script finds
and removes such image file.

The script compares image records from DB with image files from `media`
directory and removes images which are not presented in the DB. It doesn't
removes cached resized images from `cache/` directory. But it's possible to
remove all of them via cache management interface in Magento admin or by simply
removing `cache/` directory.

Copy the script file into magento root direcory and run it using php.

    php purge-unused-images.php

After the script is finished it prints some stat information.
